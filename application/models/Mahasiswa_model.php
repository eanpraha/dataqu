<?php
class Mahasiswa_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_mahasiswa($slug = FALSE)
    {
        if ($slug === FALSE) {


            $query = $this->db->get('mahasiswa');
            return $query->result_array();
        }

        $query = $this->db->get_where('mahasiswa', array('slug' => $slug));
        return $query->row_array();
    }

    public function set_mahasiswa()
    {

        $this->load->helper('url');

        $slug = url_title($this->input->post('nama'), 'dash', TRUE);
        $data = array(
            'nama' => $this->input->post('nama'),
            'jurusan' => $this->input->post('jurusan'),
            'slug' => $slug
        );

        return $this->db->insert('mahasiswa', $data);
    }

    public function get_mahasiswa_id($id  = FALSE)
    {
        $query = $this->db->get_where('mahasiswa', array('id' => $id));
        return $query->row_array();
    }

    public function update_mahasiswa($id)
    {

        $this->load->helper('url');

        $slug = url_nama($this->input->post('nama'), 'dash', TRUE);
        $data = array(
            'nama' => $this->input->post('nama'),
            'jurusan' => $this->input->post('jurusan'),
            'slug' => $slug
        );

        $this->db->where('id', $id);
        return $this->db->update('mahasiswa', $data);
    }

    public function delete_mahasiswa($id)
    {
        return $this->db->delete('mahasiswa', array('id' => $id));
        redirect('mahasiswa');
    }
}
