<!-- form left -->
<div class="container">
<div class="row">

<div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 ">
<div class="container mt-2  bg-white rounded shadow-lg">
<?php echo validation_errors(); ?>
<br>
<?php echo form_open('news/create'); ?>
    <div class="form-group">
        <label for="title">Judul</label>
        <input class="form-control border rounded border-primary" type="text" id="title" name="title" placeholder="Masukkan Judul">
    </div>

    <div class="form-group">
        <label for="text">Text</label>
        <textarea class="form-control border rounded border-primary" id="text" rows="3" name="text" placeholder="Masukkan Text"></textarea>
    </div>
    <button type="submit" name="submit" value="kirim" class="btn btn-primary">Post</button>
<?php echo form_close() ?>
<br>

</div></div>

<!-- news bar -->
<div class="col-sm-6 col-md-8 col-lg-9 col-xl-9">
<div class="row ml-4">

<?php foreach ($news as $news_item) { ?>
<div class="card text-center bg-white rounded shadow-lg m-2">
          <div class="card-body">
            <h4 class="card-title"><?php echo $news_item['title']; ?></h4>
            <p class="card-text"><?php echo $news_item['text']; ?></p>
            <p class="card-text">
            <a class="btn btn-info btn-sm" href="<?php echo site_url('news/' . $news_item['slug']); ?>" role="button">view</a>
            <a class="btn btn-dark btn-sm" href="<?php echo site_url('news/update/' . $news_item['id']); ?>" role="button">edit</a>
            <a class="btn btn-danger btn-sm" href="<?php echo site_url('news/delete/' . $news_item['id']); ?>" role="button">hapus</a>
            </p>
          </div>
        </div>
<?php } ?>
</div>
</div>

</div>
</div>
