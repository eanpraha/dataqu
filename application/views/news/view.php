<div class="container text-center mt-4">
<div class="row">
<div class="col align-self-center text-center bg-white rounded shadow-lg">
<br>
<h1><?php echo $news_item['title']; ?></h1>
<br>
<p><?php echo $news_item['text']; ?></p>
<br><br>
<a class="btn btn-info btn-sm" href="<?php echo site_url('news/'); ?>" role="button">Back</a>
<a class="btn btn-dark btn-sm" href="<?php echo site_url('news/update/' . $news_item['id']); ?>" role="button">edit</a>
<a class="btn btn-danger btn-sm" href="<?php echo site_url('news/delete/' . $news_item['id']); ?>" role="button">hapus</a>

<br><br>
</div>
</div>
