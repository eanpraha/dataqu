<div class="container m4">
    
<main role="main" class="inner cover text-center mt-5">
    <h1 class="cover-heading">Dataqu Crud CI Sample</h1>
    <p class="lead">Sample Crud News & DB Mahasiswa.</p>
    <p class="lead">
      <a href="/news" class="btn btn-lg btn-secondary">News</a>
    </p>
    <p class="lead">
      <a href="/mahasiswa" class="btn btn-lg btn-secondary">DB Mahasiswa</a>
    </p>
  </main>
  </div>
<div class="container">
  <footer class="mastfoot fixed-bottom">
    <div class="inner text-right mr-auto">
      <p>Santuy Coding for <a href="http://dataqu.apy">Dataqu</a>, by <a href="https://twitter.com/awifamz">@awifamz        </a>.</p>
    </div>
  </footer>
</div>
