<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mahasiswa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mahasiswa_model');
        $this->load->helper('url_helper');
    }


    public function index()
    {
        $data['mahasiswa'] = $this->mahasiswa_model->get_mahasiswa();
        $data['nama'] = 'Arsip Mahasiswa';

        $this->load->view('templates/header.php');
        $this->load->view('mahasiswa/index', $data);
        $this->load->view('templates/footer.php');

    }


    public function view($slug = NULL)
    {
        
        
        $data['mahasiswa_item'] = $this->mahasiswa_model->get_mahasiswa($slug);
        $this->load->view('templates/header');
        $this->load->view('mahasiswa/view', $data);
        $this->load->view('templates/footer');


    }

    public function create()
    {
        $this->load->view('templates/header');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->view('templates/footer');


        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('slug', 'nim', 'required');
        $this->form_validation->set_rules('jurusan', 'jurusan', 'required');



        if ($this->form_validation->run() === FALSE) {
            $this->load->view('mahasiswa/create');
        } else {
            $this->mahasiswa_model->set_mahasiswa();
            redirect('mahasiswa');
        }
    }


    public function update($id)
    {
        $this->load->view('templates/header');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->view('templates/footer');


        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('slug', 'nim', 'required');
        $this->form_validation->set_rules('jurusan', 'jurusan', 'required');



        if ($this->form_validation->run() === FALSE) {
            $data['mahasiswa_item'] =  $this->mahasiswa_model->get_mahasiswa_id($id);

            $this->load->view('mahasiswa/update', $data);
        } else {
            $this->mahasiswa_model->update_mahasiswa($id);
            redirect('mahasiswa');
        }
    }


    public function delete($id)
    {
        $this->mahasiswa_model->delete_mahasiswa($id);
        redirect('mahasiswa');
    }

  
}
