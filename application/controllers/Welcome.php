<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{


	public function view($halaman = 'test')
	{
		$this->load->view('templates/header');
		$this->load->view('pages/' . $halaman . '.php');
		$this->load->view('templates/footer');
	}
}
