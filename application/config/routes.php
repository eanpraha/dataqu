<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['news'] = 'news';
$route['news/create'] = 'news/create';
$route['news/update/(:any)'] = 'news/update/$1';
$route['news/delete/(:any)'] = 'news/delete/$1';
$route['news/(:any)'] = 'news/view/$1';

$route['mahasiswa'] = 'mahasiswa';
$route['mahasiswa/create'] = 'mahasiswa/create';
$route['mahasiswa/update/(:any)'] = 'mahasiswa/update/$1';
$route['mahasiswa/delete/(:any)'] = 'mahasiswa/delete/$1';
$route['mahasiswa/(:any)'] = 'mahasiswa/view/$1';



$route['default_controller'] = 'halaman/view';
$route['(:any)'] = 'halaman/view/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
